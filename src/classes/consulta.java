package classes;

import java.sql.Date;
import java.sql.Time;

public class consulta {
	
	private int id;
	private Date data;
	private Time hora;
	private String sala;
	private String observacao;
	private int idProfissional;
	private int idPaciente;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Time getHora() {
		return hora;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public int getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}
	public int getIdProfissional() {
		return idProfissional;
	}
	public void setIdProfissional(int idProfissional) {
		this.idProfissional = idProfissional;
	}
	
	
}
