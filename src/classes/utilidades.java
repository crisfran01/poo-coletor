package classes;

import java.awt.Toolkit;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import javax.swing.text.DocumentFilter.FilterBypass;

public class utilidades {
	   private static final int[] pesoCPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
	   
	
	   private static int calcularDigito(String str, int[] peso) {
	      int soma = 0;
	      for (int indice=str.length()-1, digito; indice >= 0; indice-- ) {
	         digito = Integer.parseInt(str.substring(indice,indice+1));
	         soma += digito*peso[peso.length-str.length()+indice];
	      }
	      soma = 11 - soma % 11;
	      return soma > 9 ? 0 : soma;
	   }
	
	   public static boolean isValidCPF(String cpf) {
	      if ((cpf==null) || (cpf.length()!=11)) return false;
	
	      Integer digito1 = calcularDigito(cpf.substring(0,9), pesoCPF);
	      Integer digito2 = calcularDigito(cpf.substring(0,9) + digito1, pesoCPF);
	      return cpf.equals(cpf.substring(0,9) + digito1.toString() + digito2.toString());
	   }
	   
	   public static void setFilterCPF(JTextField txt) {
		   	PlainDocument doc = (PlainDocument) txt.getDocument();
			doc.setDocumentFilter(new FiltroCPF());
	   }
	   public static void setFilterLetras(JTextField txt){
		   PlainDocument doc = (PlainDocument) txt.getDocument();
		   doc.setDocumentFilter(new FiltroApenasLetras());
	   }
	   public static void setFilterCep(JTextField txt) {
		   PlainDocument doc = (PlainDocument) txt.getDocument();
		   doc.setDocumentFilter(new FiltroCEP());
	   }
	   public static void setFilterCasa(JTextField txt) {
		   PlainDocument doc = (PlainDocument) txt.getDocument();
		   doc.setDocumentFilter(new FiltroNumeroCasa());
	   }
	   public static void setFilterRegMedico(JTextField txt) {
		   PlainDocument doc = (PlainDocument) txt.getDocument();
		   doc.setDocumentFilter(new FiltroRegMedico());
	   }
	   public static void setFilterRG(JTextField txt) {
		   PlainDocument doc = (PlainDocument) txt.getDocument();
		   doc.setDocumentFilter(new FiltroRG());
	   }
	   
}


class FiltroRegMedico extends DocumentFilter {
	final int  maxCharacters = 13;
	@Override
	public void replace(FilterBypass fb, int offs, int length,
            String str, AttributeSet a) throws BadLocationException {

        String text = fb.getDocument().getText(0,
                fb.getDocument().getLength());
        text += str;
        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                && text.matches("^[0-9]{0,10}/?[A-Z,a-z]{0,2}$")) {
            super.replace(fb, offs, length, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
	}

}

class FiltroNumeroCasa extends DocumentFilter {
	final int  maxCharacters = 6;
	@Override
	public void replace(FilterBypass fb, int offs, int length,
            String str, AttributeSet a) throws BadLocationException {

        String text = fb.getDocument().getText(0,
                fb.getDocument().getLength());
        text += str;
        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                && text.matches("^[0-9]+[0-9,A-Z,a-z]{0,1}$")) {
            super.replace(fb, offs, length, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
	}
}
class FiltroCPF extends DocumentFilter {
		final int  maxCharacters = 11;
		@Override
		public void replace(FilterBypass fb, int offs, int length,
                String str, AttributeSet a) throws BadLocationException {

            String text = fb.getDocument().getText(0,
                    fb.getDocument().getLength());
            text += str;
            if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                    && text.matches("^[0-9]+$")) {
                super.replace(fb, offs, length, str, a);
            } else {
                Toolkit.getDefaultToolkit().beep();
            }
	}
}


class FiltroCEP extends DocumentFilter {
	final int  maxCharacters = 8;
	@Override
	public void replace(FilterBypass fb, int offs, int length,
            String str, AttributeSet a) throws BadLocationException {

        String text = fb.getDocument().getText(0,
                fb.getDocument().getLength());
        text += str;
        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                && text.matches("[0-9]+$")) {
            super.replace(fb, offs, length, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
}
}


class FiltroApenasLetras extends DocumentFilter {
	final int  maxCharacters = 45;
	@Override
	public void replace(FilterBypass fb, int offs, int length,
            String str, AttributeSet a) throws BadLocationException {

        String text = fb.getDocument().getText(0,
                fb.getDocument().getLength());
        text += str;
        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                && text.matches("^[A-Za-z\\s]{1,}{0,1}[A-Za-z\\s]{0,}$")) {
            super.replace(fb, offs, length, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
}
}

class FiltroRG extends DocumentFilter {
	final int  maxCharacters = 14;
	@Override
	public void replace(FilterBypass fb, int offs, int length,
            String str, AttributeSet a) throws BadLocationException {

        String text = fb.getDocument().getText(0,
                fb.getDocument().getLength());
        text += str;
        if ((fb.getDocument().getLength() + str.length() - length) <= maxCharacters
                && text.matches("^[0-9]+[0-9,X,x]{0,1}$")) {
            super.replace(fb, offs, length, str, a);
        } else {
            Toolkit.getDefaultToolkit().beep();
        }
	}
}
