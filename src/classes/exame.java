package classes;

public class exame {
	private int idExame;
	private int idPaciente;
	private int idProfissionalDeSaude;
	private String data;
	private String tipo;
	private String observacao;
	private int idUsuario;
	private String nomePaciente;
	
	public int getIdExame() {
		return idExame;
	}
	public void setIdExame(int idExame) {
		this.idExame = idExame;
	}
	public int getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}
	public int getIdProfissionalDeSaude() {
		return idProfissionalDeSaude;
	}
	public void setIdProfissionalDeSaude(int idProfissionalDeSaude) {
		this.idProfissionalDeSaude = idProfissionalDeSaude;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNomePaciente() {
		return nomePaciente;
	}
	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}
}
