package classes;

public class Prontuario {
	private int idregistroProntuario;
	private int idPaciente;
	private int idProfissionaldasaude;
	private String resumo;
	private String anotacoes;
	private String nomePaciente;
	
	public int getIdregistroProntuario() {
		return idregistroProntuario;
	}
	public void setIdregistroProntuario(int idregistroProntuario) {
		this.idregistroProntuario = idregistroProntuario;
	}
	public int getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(int idPaciente) {
		this.idPaciente = idPaciente;
	}
	public int getIdProfissionaldasaude() {
		return idProfissionaldasaude;
	}
	public void setIdProfissionaldasaude(int idProfissionaldasaude) {
		this.idProfissionaldasaude = idProfissionaldasaude;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getAnotacoes() {
		return anotacoes;
	}
	public void setAnotacoes(String anotacoes) {
		this.anotacoes = anotacoes;
	}
	public String getNomePaciente() {
		return nomePaciente;
	}
	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}
	
}
