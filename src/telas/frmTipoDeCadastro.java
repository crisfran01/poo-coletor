package telas;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.UsuarioDao;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class frmTipoDeCadastro extends JFrame {
	
	public void CloseFrame(){
	    this.dispose();
	}

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmTipoDeCadastro frame = new frmTipoDeCadastro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmTipoDeCadastro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 456, 339);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		JButton btnLogIn = new JButton("Cadastrar Coletor");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCadastraColetor form = new frmCadastraColetor(true);
				form.setVisible(true);
			
				CloseFrame();
			}
			
		});
		btnLogIn.setFont(new Font("Khmer OS", Font.BOLD, 14));
		btnLogIn.setBounds(124, 83, 210, 23);
		jpanel.add(btnLogIn);
		
		JButton btnCadastro = new JButton("Cadastrar Usuário");
		btnCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCadastroUsuário form = new frmCadastroUsuário(true);
				form.setVisible(true);
			
				CloseFrame();
			}
		});
		btnCadastro.setFont(new Font("Khmer OS", Font.BOLD, 14));
		btnCadastro.setBounds(124, 158, 210, 23);
		jpanel.add(btnCadastro);
	}
}
