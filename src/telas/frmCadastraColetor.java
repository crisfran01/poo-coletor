package telas;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.bind.ParseConversionEvent;

import classes.Coletor;
import classes.utilidades;
import dao.ColetorDao;
import javax.swing.text.PlainDocument;

import javax.swing.JTextField;
import java.awt.SystemColor;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class frmCadastraColetor extends JFrame {
	
	//Habilita ou desabilita os campos de texto
	public void enableFields(boolean enable, JComboBox<?> cbSexo, JComboBox<?> cbUF, JComboBox<?> cbMaterial) {
		txtNome.setEditable(enable);
		txtCpf.setEditable(enable);  
		txtRg.setEditable(enable);  
		txtRua.setEditable(enable);  
		txtNumero.setEditable(enable);
		txtCEP.setEditable(enable);  
		txtCidade.setEditable(enable);
		txtBairro.setEditable(enable);
		txtCEP.setEditable(enable);
		cbSexo.setEnabled(enable);
		cbUF.setEnabled(enable);
		
		cbSexo.setRenderer(new DefaultListCellRenderer() {
	        @Override
	        public void paint(Graphics g) {
	            setForeground(Color.BLACK);
	            super.paint(g);
	        }
	        });
		cbUF.setRenderer(new DefaultListCellRenderer() {
	        @Override
	        public void paint(Graphics g) {
	            setForeground(Color.BLACK);
	            super.paint(g);
	        }
	        });
		cbMaterial.setRenderer(new DefaultListCellRenderer() {
	        @Override
	        public void paint(Graphics g) {
	            setForeground(Color.BLACK);
	            super.paint(g);
	        }
	        });
	}
	
	
	// Limpa os campos de texto
	public void clearFields(JComboBox<?> cbSexo, JComboBox<?> cbUF, JComboBox<?> cbMaterial) {
		txtNome.setText("");
		txtCpf.setText("");
		cbSexo.setSelectedIndex(0);
		txtRg.setText(""); 
		txtRua.setText(""); 
		txtNumero.setText("");
		cbUF.setSelectedIndex(0); 
		txtCEP.setText(""); 
		txtCidade.setText("");;
		txtBairro.setText("");;
		txtCEP.setText("");
		cbMaterial.setSelectedIndex(0); 
		System.out.println("clear");
	}
	
	// Variaveis para controle dos dados do banco de dados
	private List<Coletor> coletores = new ArrayList<Coletor>();
	private int index = 0;
	boolean update = false; 
	
	// Preenche os campos com os dados do bd
	public void fillFields(int index, JComboBox<?> cbSexo, JComboBox<?> cbUF, JComboBox<?> cbMaterial ) {
		Coletor p = new Coletor();
		p = coletores.get(index);
		txtId.setText(Integer.toString(p.getId()));
		txtNome.setText(p.getNome());
		txtCpf.setText(p.getCpf()); 
		cbSexo.setSelectedItem(p.getSexo());
		txtRg.setText(p.getRg()); 
		txtRua.setText(p.getRua()); 
		txtNumero.setText( Integer.toString(p.getNumero()));
		cbUF.setSelectedItem(p.getUf()); 
		txtCEP.setText(p.getCep()); 
		txtCidade.setText(p.getCidade());;
		txtBairro.setText(p.getBairro());;
		txtCEP.setText(p.getCep());;
		cbMaterial.setSelectedItem(p.getMaterial()); 
		}
	

	
	// Procura a posição do paciente recebido na busca dentro dos dados internos 
	public void searchID(int id) {
		int i = 0;
		for(Coletor p : coletores){
			if(p.getId() == id) {
				index = i;
				break;
			}
			i++;
		}
	}
	
	// Metodo que permitem campo receber apenas número nos campos de texto
	public void onlyNumbers(java.awt.event.KeyEvent evt) {
		String c= String.valueOf( evt.getKeyChar());	        	
        if(!c.matches("[0-9]*")){
               evt.consume();
        }
	}

 
	// Text Filds
	private JComboBox cbSexo;
	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtNome;
	private JTextField txtCpf;
	private JTextField txtRua;
	private JTextField txtRg;
	private JTextField txtNumero;
	private JTextField txtCidade;
	private JTextField txtCEP;
	private JTextField txtBairro;
	private JTextField txtEmail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmCadastraColetor frame = new frmCadastraColetor(false);
					frame.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmCadastraColetor(boolean isNew) {
		ColetorDao bd = new ColetorDao();
		coletores = bd.get();
		
		// Declaração da tela
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 990, 550);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		
		// Declaração dos botões
		
		JButton btnAnterior = new JButton("Anterior");
		JButton btnProx = new JButton("Pr\u00F3ximo");
		JButton btnEdit = new JButton("");
		JButton btnAdd = new JButton("");
		JButton btnSave = new JButton("");
		JButton btnCancel = new JButton("");

		// Desingn dos elementos
		
		txtId = new JTextField();
		txtId.setBounds(233, 110, 68, 24);
		txtId.setEditable(false);
		jpanel.add(txtId);
		
		txtNome = new JTextField();
		txtNome.setBounds(349, 110, 408, 24);
		jpanel.add(txtNome);
		utilidades.setFilterLetras(txtNome);
		GridBagLayout gbl_txtNome = new GridBagLayout();
		gbl_txtNome.columnWidths = new int[]{0};
		gbl_txtNome.rowHeights = new int[]{0};
		gbl_txtNome.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_txtNome.rowWeights = new double[]{Double.MIN_VALUE};
		txtNome.setLayout(gbl_txtNome);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(233, 156, 254, 24);
		jpanel.add(txtCpf);
		utilidades.setFilterCPF(txtCpf);
		
		txtRg = new JTextField();
		txtRg.setBounds(548, 156, 315, 24);
		jpanel.add(txtRg);
		
		txtRua = new JTextField();
		txtRua.setBounds(233, 200, 326, 24);
		jpanel.add(txtRua);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(633, 200, 124, 24);
		jpanel.add(txtNumero);
		utilidades.setFilterCasa(txtNumero);
		
		txtCEP = new JTextField();
		txtCEP.setBounds(233, 242, 105, 24);
		jpanel.add(txtCEP);
		utilidades.setFilterCep(txtCEP);
		
		txtCidade = new JTextField();
		txtCidade.setBounds(400, 242, 160, 24);
		jpanel.add(txtCidade);
		
		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblId.setForeground(Color.WHITE);
		lblId.setBounds(111, 109, 46, 26);
		jpanel.add(lblId);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblNome.setForeground(Color.WHITE);
		lblNome.setBounds(307, 109, 46, 26);
		jpanel.add(lblNome);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblSexo.setForeground(Color.WHITE);
		lblSexo.setBounds(767, 109, 46, 26);
		jpanel.add(lblSexo);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCpf.setForeground(Color.WHITE);
		lblCpf.setBounds(111, 156, 46, 25);
		jpanel.add(lblCpf);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblRg.setForeground(Color.WHITE);
		lblRg.setBounds(513, 155, 46, 26);
		jpanel.add(lblRg);
		
		JLabel lblRua = new JLabel("Rua");
		lblRua.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblRua.setForeground(Color.WHITE);
		lblRua.setBounds(111, 199, 46, 26);
		jpanel.add(lblRua);
		
		JLabel lblNmero = new JLabel("N\u00FAmero");
		lblNmero.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblNmero.setForeground(Color.WHITE);
		lblNmero.setBounds(573, 199, 59, 26);
		jpanel.add(lblNmero);
		
		JLabel lblUf = new JLabel("UF");
		lblUf.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblUf.setForeground(Color.WHITE);
		lblUf.setBounds(767, 199, 46, 26);
		jpanel.add(lblUf);
		
		JLabel lblCep = new JLabel("CEP");
		lblCep.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCep.setForeground(Color.WHITE);
		lblCep.setBounds(111, 241, 46, 26);
		jpanel.add(lblCep);
		
		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCidade.setForeground(Color.WHITE);
		lblCidade.setBounds(343, 241, 59, 26);
		jpanel.add(lblCidade);
		
		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblBairro.setForeground(Color.WHITE);
		lblBairro.setBounds(570, 241, 59, 26);
		jpanel.add(lblBairro);
		
		
		btnAnterior.setBounds(111, 396, 124, 24);
		jpanel.add(btnAnterior);
		
		btnProx.setBounds(724, 396, 139, 24);
		jpanel.add(btnProx);
		

		btnEdit.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/pencil-edit-button.png")));
		btnEdit.setBackground(new Color(40, 40,40));
		btnEdit.setBounds(509, 391, 35, 35);
		jpanel.add(btnEdit);
		
		btnAdd.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/add.png")));
		btnAdd.setBackground(new Color(40, 40,40));
		
	
		btnAdd.setBounds(462, 391, 35, 35);
		jpanel.add(btnAdd);
		
		btnSave.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/save.png")));
		btnSave.setBackground(new Color(40, 40,40));
		btnSave.setBounds(462, 391, 35, 35);
		jpanel.add(btnSave);
		
		
		btnCancel.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/close.png")));
		btnCancel.setBackground(new Color(40, 40, 40));
		btnCancel.setBounds(509, 391, 35, 35);
		jpanel.add(btnCancel);
		
		JLabel lblCadastroDePacientes = new JLabel("Cadastro de Instituição");
		lblCadastroDePacientes.setForeground(Color.WHITE);
		lblCadastroDePacientes.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCadastroDePacientes.setBounds(403, 12, 173, 45);
		jpanel.add(lblCadastroDePacientes);
		
		JLabel lblCdigoDoConvenio = new JLabel("Tipo de Material");
		lblCdigoDoConvenio.setForeground(Color.WHITE);
		lblCdigoDoConvenio.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCdigoDoConvenio.setBounds(54, 291, 160, 24);
		jpanel.add(lblCdigoDoConvenio);
		
		txtBairro = new JTextField();
		txtBairro.setBounds(643, 242, 220, 24);
		jpanel.add(txtBairro);
		
		JComboBox cbSexo = new JComboBox();
		cbSexo.setBackground(Color.WHITE);
		cbSexo.setBounds(817, 112, 46, 24);
		jpanel.add(cbSexo);
		cbSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "F", "M"}));
		
		JComboBox cbUF = new JComboBox();
		cbUF.setBackground(Color.WHITE);
		cbUF.setBounds(811, 202, 52, 24);
		cbUF.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RS", "SC", "SE", "SP", "TO"}));
		jpanel.add(cbUF);
		
		JComboBox<?> cbMaterial = new JComboBox();
		cbMaterial.setModel(new DefaultComboBoxModel(new String[] {"Vidro", "Plastico", "Metal", "Papel"}));
		cbMaterial.setEnabled(false);
		cbMaterial.setBackground(Color.WHITE);
		cbMaterial.setBounds(232, 290, 631, 24);
		jpanel.add(cbMaterial);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(242, 326, 621, 24);
		jpanel.add(txtEmail);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblEmail.setBounds(54, 327, 160, 24);
		jpanel.add(lblEmail);
	
		
		// Permitir apenas números - Cris eu te odeio, fiz melhor q vc e vc me vem e me ofende fazendo isso >:(
	/*	txtNumero.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {
	        	onlyNumbers(evt);
	        }
	    });
		
		txtCpf.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {
	        	onlyNumbers(evt);
	        }
	    });
		
		txtConvenio.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {
	        	onlyNumbers(evt);
	        }
	    });
		
		txtRg.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {
	        	String c= String.valueOf( evt.getKeyChar());	        	
	            if(!c.matches("[0-9 | x | X]")){
	                   evt.consume();
	            }
	        }
	    });	
		
		txtCEP.addKeyListener(new java.awt.event.KeyAdapter() {
	        public void keyTyped(java.awt.event.KeyEvent evt) {
	        	onlyNumbers(evt);
	        }
	    });
		*/
		// Listeners dos botões		
		
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (index > 0) {
					index --;
					fillFields(index, cbSexo, cbUF, cbMaterial);
					
				}
			}
		});
		
		btnProx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(index != coletores.size() - 1 ) {
					index++;
					fillFields(index, cbSexo, cbUF, cbMaterial);
					
				}
				
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fillFields(index, cbSexo,cbUF, cbMaterial);
				enableFields(false,cbSexo,cbUF, cbMaterial);
				btnAnterior.setVisible(true);
				btnProx.setVisible(true);
				btnAdd.setVisible(true);
				btnEdit.setVisible(true);	
				btnSave.setVisible(false);
				btnCancel.setVisible(false);
			}
		});
		
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update = true;
				enableFields(true,cbSexo,cbUF, cbMaterial);
				btnAnterior.setVisible(false);
				btnProx.setVisible(false);
				btnAdd.setVisible(false);
				btnEdit.setVisible(false);	
				btnSave.setVisible(true);
				btnCancel.setVisible(true);
				
			}
		});
		
		
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean validCPF = utilidades.isValidCPF(txtCpf.getText()); //VALIDAR CPF AQUI
				//boolean validCEP = true; // VALIDAR cEP AQUI 
				if(validCPF){
					if(txtNome.getText() == "" || txtRua.getText() == "" || txtCidade.getText() == "" || txtNumero.getText() == "" || txtEmail.getText() == "") {
						
						Coletor p = new Coletor();
						p.setNome(txtNome.getText());
						p.setSexo(cbSexo.getSelectedItem().toString());
						p.setCpf(txtCpf.getText());
						p.setRg(txtRg.getText());
						p.setRua(txtRua.getText());
						p.setNumero( Integer.parseInt(txtNumero.getText()));
						p.setUf(cbUF.getSelectedItem().toString());
						p.setCidade(txtCidade.getText());
						p.setCep(txtCEP.getText());
						p.setBairro(txtBairro.getText());
						p.setMaterial(cbMaterial.getSelectedItem().toString());
						ColetorDao db = new ColetorDao();
						p.setId(Integer.parseInt(txtId.getText()));
						if(update) {
							db.update(p);
							update = false;
						}else {
							db.inserir(p);
							index = coletores.size();
						}
						enableFields(false,cbSexo,cbUF, cbMaterial);
						btnAnterior.setVisible(true);
						btnProx.setVisible(true);
						btnAdd.setVisible(true);
						btnEdit.setVisible(true);	
						btnSave.setVisible(false);
						btnCancel.setVisible(false);
						coletores = db.get();
					}else {
						JOptionPane.showMessageDialog(null, "Há campos obrigatórios vazios");
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "CPF Invalido");
				}
			}
		});
		
		
		
		// Seta visibilidade inicial dos botões
		
		btnCancel.setVisible(false);
		btnSave.setVisible(false);
		btnEdit.setVisible(true);
		btnAdd.setVisible(false);
		
		//SE for Cadastro novo
		
		if(isNew) {
			clearFields(cbSexo,cbUF, cbMaterial);
			enableFields(true,cbSexo,cbUF, cbMaterial);	
			txtId.setText( Integer.toString(coletores.size()+1));
			btnAnterior.setVisible(false);
			btnProx.setVisible(false);
			btnEdit.setVisible(false);	
			btnSave.setVisible(true);
			btnCancel.setVisible(true);
		}else {
			// Desabilita os campos de texto
			enableFields(false,cbSexo,cbUF, cbMaterial);			
			
			// Carrega os dados dos coletores se houver
			if(coletores.size() >0 ) {
				fillFields(index,cbSexo,cbUF, cbMaterial);	
				
			}			
		}
		
		
		
		
		
	}
}
