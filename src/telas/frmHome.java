package telas;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import java.awt.Color;

public class frmHome extends JFrame {

	private JPanel contentPane;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmHome frame = new frmHome();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmHome() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 990, 550);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		contentPane = new JPanel();
		contentPane.setForeground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(40, 40, 40));
		contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		JLabel lblLogo = new JLabel(new ImageIcon("/home/cristiane/Desktop/health.png"));
		lblLogo.setBackground(new Color(204, 51, 204));
		lblLogo.setBounds(74, 105, 852, 277);
		jpanel.add(lblLogo);
	
		
		
		menuBar.setMargin(new Insets(10, 0, 10, 0));
		menuBar.setBorder(BorderFactory.createCompoundBorder(menuBar.getBorder(),BorderFactory.createEmptyBorder(10, 0, 05, 0)));		
		menuBar.setBackground(new Color(40, 40, 40));
		menuBar.setFont(new Font("Khmer OS", Font.BOLD, 14));
		
	
		JMenu mnPerfil = new JMenu("Perfil");
		mnPerfil.setForeground(Color.WHITE);
		mnPerfil.setFont(new Font("Khmer OS", Font.BOLD, 14));
		mnPerfil.setBackground(new Color(40, 40, 40));
		menuBar.add(mnPerfil);
		
		JMenuItem mntmVerDoaes = new JMenuItem("Ver Doações");
		mntmVerDoaes.setForeground(Color.WHITE);
		mntmVerDoaes.setBackground(new Color(40, 40, 40));
		mnPerfil.add(mntmVerDoaes);
		
		JMenuItem mntmCadastrarDoaes = new JMenuItem("Cadastrar Doações");
		mntmCadastrarDoaes.setForeground(Color.WHITE);
		mntmCadastrarDoaes.setBackground(new Color(40, 40, 40));
		mnPerfil.add(mntmCadastrarDoaes);
		
		JMenuItem menuItem_1 = new JMenuItem("Editar informações");
		menuItem_1.setForeground(Color.WHITE);
		menuItem_1.setBackground(new Color(40, 40, 40));
		mnPerfil.add(menuItem_1);
		
		JMenu mnSistema = new JMenu("Sistema");
		mnSistema.setBackground(new Color(40,40,40));
		mnSistema.setForeground(SystemColor.controlLtHighlight);
		mnSistema.setFont(new Font("Khmer OS", Font.BOLD, 14));
		menuBar.add(mnSistema);
		
	
		

	
		
	
		
		
		
	}

}
