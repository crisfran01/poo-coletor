package telas;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class frmLogin extends JFrame {
	
	public void CloseFrame(){
	    this.dispose();
	}

	private JPanel contentPane;
	private JTextField txtLogin;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmLogin frame = new frmLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 456, 339);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		txtLogin = new JTextField();
		txtLogin.setBounds(150, 37, 188, 24);
		jpanel.add(txtLogin);
		txtLogin.setColumns(10);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(150, 116, 188, 24);
		jpanel.add(txtSenha);
		
		JLabel lblSenha = new JLabel("Senha");
		JLabel lblerror = new JLabel("Usuário ou Senha incorretos");
		JLabel lblLogin = new JLabel("E-mail");
		
		lblLogin.setForeground(Color.WHITE);
		lblLogin.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblLogin.setBounds(80, 37, 46, 21);
		jpanel.add(lblLogin);
		
		lblSenha.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblSenha.setForeground(Color.WHITE);
		lblSenha.setBounds(80, 119, 46, 14);
		jpanel.add(lblSenha);
		
		JButton btnLogIn = new JButton("Iniciar Sess\u00E3o");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
			
		});
		btnLogIn.setFont(new Font("Khmer OS", Font.BOLD, 14));
		btnLogIn.setBounds(148, 178, 140, 23);
		jpanel.add(btnLogIn);
		
		lblerror.setForeground(Color.WHITE);
		lblerror.setFont(new Font("Khmer OS", Font.BOLD, 12));
		lblerror.setBounds(126, 201, 183, 14);
		jpanel.add(lblerror);
		
		JLabel lblRecoverPass = new JLabel("Esqueci minha senha");
		JLabel lblAltSenha = new JLabel("Digite seu nome de usuário e clique novamente ");
		lblRecoverPass.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(txtLogin.getText() == "") {
					lblAltSenha.setVisible(true);
					
				}else {
					lblAltSenha.setVisible(false);
				}
			}
		});
		lblRecoverPass.setForeground(Color.WHITE);
		lblRecoverPass.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblRecoverPass.setBounds(268, 273, 166, 14);
		jpanel.add(lblRecoverPass);
		
		lblAltSenha.setForeground(Color.WHITE);
		lblAltSenha.setFont(new Font("Khmer OS", Font.BOLD, 12));
		lblAltSenha.setBounds(66, 73, 314, 14);
		jpanel.add(lblAltSenha);
		
		JButton btnCadastro = new JButton("Cadastrar");
		btnCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmTipoDeCadastro form = new frmTipoDeCadastro();
				form.setVisible(true);
			
				CloseFrame();
			}
		});
		btnCadastro.setFont(new Font("Khmer OS", Font.BOLD, 14));
		btnCadastro.setBounds(150, 213, 140, 23);
		jpanel.add(btnCadastro);
		
		lblAltSenha.setVisible(false);
		lblerror.setVisible(false);
	}
}
