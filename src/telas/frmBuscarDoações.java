package telas;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.SystemColor;
import javax.swing.DefaultComboBoxModel;

public class frmBuscarDoações extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmBuscarDoações frame = new frmBuscarDoações();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmBuscarDoações() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 990, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
	contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		JButton btnNewButton_1 = new JButton("Declarar interesse");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setBounds(546, 436, 301, 24);
		jpanel.add(btnNewButton_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Tipo de material", "Bairro", "Cidade", "Rua"}));
		comboBox.setBackground(SystemColor.text);
		comboBox.setBounds(225, 82, 212, 24);
		jpanel.add(comboBox);
		
		JLabel lblBuscar = new JLabel("Buscar");
		lblBuscar.setForeground(Color.WHITE);
		lblBuscar.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblBuscar.setBounds(155, 85, 63, 14);
		jpanel.add(lblBuscar);
		
		JTextField textField = new JTextField();
		textField.setBounds(532, 83, 315, 24);
		jpanel.add(textField);
		textField.setColumns(10);
		
		JTable table = new JTable();
		table.setBounds(150, 118, 697, 295);
		jpanel.add(table);
	}

}
