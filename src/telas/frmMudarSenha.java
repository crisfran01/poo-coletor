package telas;


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dao.UsuarioDao;

import java.awt.Color;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class frmMudarSenha extends JFrame {
	
	public void CloseFrame(){
	    this.dispose();
	}


	private JPanel contentPane;
	private JPasswordField txtSenhaNC;
	private JLabel lblSenhaAtual;
	private JPasswordField txtSenhaA;
	private JLabel lblNovasenha;
	private JPasswordField txtSenhaN;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmMudarSenha frame = new frmMudarSenha();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmMudarSenha() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLoogin = new JLabel("Confirmar Nova Senha");
		lblLoogin.setForeground(SystemColor.text);
		lblLoogin.setBounds(142, 150, 165, 15);
		contentPane.add(lblLoogin);
		
		txtSenhaNC = new JPasswordField();
		txtSenhaNC.setBounds(87, 173, 268, 24);
		contentPane.add(txtSenhaNC);
		
		JButton btnNewButton = new JButton("Alterar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(String.valueOf(txtSenhaA.getPassword()).equals("")) {
					if(txtSenhaN.getText().length() >= 6) {
						if(txtSenhaN.getText().equals(txtSenhaNC.getText())) {
							UsuarioDao bd = new UsuarioDao();
							CloseFrame();
						}else {
							JOptionPane.showMessageDialog(null, "Senhas nova não é igual a confirmação");
						}
						
					}else {
						JOptionPane.showMessageDialog(null, "Senha muito curta");
					}
				}else {
					JOptionPane.showMessageDialog(null, "Senhas Atual não confere");
				}
			}
		});
		btnNewButton.setBounds(166, 216, 117, 25);
		contentPane.add(btnNewButton);
		
		lblSenhaAtual = new JLabel("Senha Atual");
		lblSenhaAtual.setForeground(Color.WHITE);
		lblSenhaAtual.setBounds(178, 25, 92, 15);
		contentPane.add(lblSenhaAtual);
		
		txtSenhaA = new JPasswordField();
		txtSenhaA.setBounds(87, 48, 268, 24);
		contentPane.add(txtSenhaA);
		
		lblNovasenha = new JLabel("Nova Senha");
		lblNovasenha.setForeground(Color.WHITE);
		lblNovasenha.setBounds(181, 84, 86, 15);
		contentPane.add(lblNovasenha);
		
		txtSenhaN = new JPasswordField();
		txtSenhaN.setBounds(87, 107, 268, 24);
		contentPane.add(txtSenhaN);
	}
}
