package telas;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.PlainDocument;

import classes.Usuário;
import classes.utilidades;
import dao.UsuarioDao;

import java.awt.SystemColor;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;

public class frmCadastroUsuário extends JFrame {
	
	//Habilita ou desabilita os campos de texto
	public void enableFields(boolean enable, JComboBox<?> cbSexo, JComboBox<?> cbUF) {
		txtNome.setEditable(enable);
		txtCpf.setEditable(enable);  
		txtRg.setEditable(enable);  
		txtRua.setEditable(enable);  
		txtNumero.setEditable(enable);
		txtCEP.setEditable(enable);  
		txtCidade.setEditable(enable);
		txtBairro.setEditable(enable);
		txtCEP.setEditable(enable);
		cbSexo.setEnabled(enable);
		cbUF.setEnabled(enable);
		
		cbSexo.setRenderer(new DefaultListCellRenderer() {
	        @Override
	        public void paint(Graphics g) {
	            setForeground(Color.BLACK);
	            super.paint(g);
	        }
	        });
		cbUF.setRenderer(new DefaultListCellRenderer() {
	        @Override
	        public void paint(Graphics g) {
	            setForeground(Color.BLACK);
	            super.paint(g);
	        }
	        });
	}
	
	// Limpa os campos de texto
	public void clearFields(JComboBox<?> cbSexo, JComboBox<?> cbUF) {
		txtNome.setText("");
		txtCpf.setText("");
		cbSexo.setSelectedIndex(0);
		txtRg.setText(""); 
		txtRua.setText(""); 
		txtNumero.setText("");
		cbUF.setSelectedIndex(0); 
		txtCEP.setText(""); 
		txtCidade.setText("");
		txtBairro.setText("");
		txtCEP.setText("");
		System.out.println("clear");
	}
	
	// Variaveis para controle dos dados do banco de dados
	private List<Usuário> funcionarios = new ArrayList<Usuário>();
	private int index = 0;
	boolean update = false; 
	
	// Preenche os campos com os dados do bd
	public void fillFields(int index, JComboBox<?> cbSexo, JComboBox<?> cbUF) {
		Usuário f = new Usuário();
		f = funcionarios.get(index);
		txtId.setText(Integer.toString(f.getId()));
		txtNome.setText(f.getNome());
		txtCpf.setText(f.getCpf()); 
		cbSexo.setSelectedItem(f.getSexo());
		txtRg.setText(f.getRg()); 
		txtRua.setText(f.getRua()); 
		txtNumero.setText( Integer.toString(f.getNumero()));
		cbUF.setSelectedItem(f.getUf()); 
		txtCEP.setText(f.getCep()); 
		txtCidade.setText(f.getCidade());;
		txtBairro.setText(f.getBairro());;
		txtCEP.setText(f.getCep());;

	}
	

	
	// Procura a posição do funcionario recebido na busca dentro dos dados internos 
	public void searchID(int id) {
		int i = 0;
		for(Usuário f : funcionarios){
			if(f.getId() == id) {
				index = i;
				break;
			}
			i++;
		}
	}
	
	// Metodo que permitem campo receber apenas número nos campos de texto
	public void onlyNumbers(java.awt.event.KeyEvent evt) {
		String c= String.valueOf( evt.getKeyChar());	        	
        if(!c.matches("[0-9]*")){
               evt.consume();
        }
	}

	
	// Text Filds
		private JComboBox cbSexo;
		private JPanel contentPane;
		private JTextField txtId;
		private JTextField txtNome;
		private JTextField txtCpf;
		private JTextField txtRua;
		private JTextField txtRg;
		private JTextField txtNumero;
		private JTextField txtCidade;
		private JTextField txtCEP;
		private JTextField txtBairro;
		private JTextField txtEmail;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmCadastroUsuário frame = new frmCadastroUsuário(false);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public frmCadastroUsuário(boolean isNew) {
		UsuarioDao bd = new UsuarioDao();
		funcionarios = bd.get();
		
		// Declaração da tela
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 990, 550);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(40, 40, 40));
		
		JPanel jpanel = new JPanel();
		jpanel.setBackground(new Color(40, 40, 40));
		contentPane.add(jpanel, BorderLayout.CENTER);
		jpanel.setLayout(null);
		
		
		// Declaração dos botões
		
		JButton btnAnterior = new JButton("Anterior");
		JButton btnProx = new JButton("Pr\u00F3ximo");
		JButton btnEdit = new JButton("");
		JButton btnAdd = new JButton("");
		JButton btnSave = new JButton("");
		JButton btnBusca = new JButton("");
		JButton btnCancel = new JButton("");

		// Desingn dos elementos
		
		txtId = new JTextField();
		txtId.setBounds(233, 110, 68, 24);
		txtId.setEditable(false);
		jpanel.add(txtId);
		
		txtNome = new JTextField();
		txtNome.setBounds(349, 110, 408, 24);
		jpanel.add(txtNome);
		utilidades.setFilterLetras(txtNome);
		GridBagLayout gbl_txtNome = new GridBagLayout();
		gbl_txtNome.columnWidths = new int[]{0};
		gbl_txtNome.rowHeights = new int[]{0};
		gbl_txtNome.columnWeights = new double[]{Double.MIN_VALUE};
		gbl_txtNome.rowWeights = new double[]{Double.MIN_VALUE};
		txtNome.setLayout(gbl_txtNome);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(233, 156, 254, 24);
		jpanel.add(txtCpf);
		utilidades.setFilterCPF(txtCpf);
		
		txtRg = new JTextField();
		txtRg.setBounds(548, 156, 315, 24);
		jpanel.add(txtRg);
		
		txtRua = new JTextField();
		txtRua.setBounds(233, 200, 326, 24);
		jpanel.add(txtRua);
		
		txtNumero = new JTextField();
		txtNumero.setBounds(633, 200, 124, 24);
		jpanel.add(txtNumero);
		utilidades.setFilterCasa(txtNumero);
		
		txtCEP = new JTextField();
		txtCEP.setBounds(233, 242, 105, 24);
		jpanel.add(txtCEP);
		utilidades.setFilterCep(txtCEP);
		
		txtCidade = new JTextField();
		txtCidade.setBounds(400, 242, 160, 24);
		jpanel.add(txtCidade);
		
		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblId.setForeground(Color.WHITE);
		lblId.setBounds(111, 109, 46, 26);
		jpanel.add(lblId);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblNome.setForeground(Color.WHITE);
		lblNome.setBounds(307, 109, 46, 26);
		jpanel.add(lblNome);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblSexo.setForeground(Color.WHITE);
		lblSexo.setBounds(767, 109, 46, 26);
		jpanel.add(lblSexo);
		
		JLabel lblCpf = new JLabel("CPF");
		lblCpf.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCpf.setForeground(Color.WHITE);
		lblCpf.setBounds(111, 156, 46, 25);
		jpanel.add(lblCpf);
		
		JLabel lblRg = new JLabel("RG");
		lblRg.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblRg.setForeground(Color.WHITE);
		lblRg.setBounds(513, 155, 46, 26);
		jpanel.add(lblRg);
		
		JLabel lblRua = new JLabel("Rua");
		lblRua.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblRua.setForeground(Color.WHITE);
		lblRua.setBounds(111, 199, 46, 26);
		jpanel.add(lblRua);
		
		JLabel lblNmero = new JLabel("N\u00FAmero");
		lblNmero.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblNmero.setForeground(Color.WHITE);
		lblNmero.setBounds(573, 199, 59, 26);
		jpanel.add(lblNmero);
		
		JLabel lblUf = new JLabel("UF");
		lblUf.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblUf.setForeground(Color.WHITE);
		lblUf.setBounds(767, 199, 46, 26);
		jpanel.add(lblUf);
		
		JLabel lblCep = new JLabel("CEP");
		lblCep.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCep.setForeground(Color.WHITE);
		lblCep.setBounds(111, 241, 46, 26);
		jpanel.add(lblCep);
		
		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCidade.setForeground(Color.WHITE);
		lblCidade.setBounds(343, 241, 59, 26);
		jpanel.add(lblCidade);
		
		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblBairro.setForeground(Color.WHITE);
		lblBairro.setBounds(570, 241, 59, 26);
		jpanel.add(lblBairro);
		
		
		btnAnterior.setBounds(111, 396, 124, 24);
		jpanel.add(btnAnterior);
		
		btnProx.setBounds(724, 396, 139, 24);
		jpanel.add(btnProx);
		

		btnEdit.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/pencil-edit-button.png")));
		btnEdit.setBackground(new Color(40, 40,40));
		btnEdit.setBounds(509, 391, 35, 35);
		jpanel.add(btnEdit);
		
		btnAdd.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/add.png")));
		btnAdd.setBackground(new Color(40, 40,40));
		
	
		btnAdd.setBounds(462, 391, 35, 35);
		jpanel.add(btnAdd);
		
		btnSave.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/save.png")));
		btnSave.setBackground(new Color(40, 40,40));
		btnSave.setBounds(462, 391, 35, 35);
		jpanel.add(btnSave);
		
		
		btnCancel.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/close.png")));
		btnCancel.setBackground(new Color(40, 40, 40));
		btnCancel.setBounds(509, 391, 35, 35);
		jpanel.add(btnCancel);
		
		JLabel lblCadastroDePacientes = new JLabel("Cadastro de usuário");
		lblCadastroDePacientes.setForeground(Color.WHITE);
		lblCadastroDePacientes.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblCadastroDePacientes.setBounds(403, 12, 173, 45);
		jpanel.add(lblCadastroDePacientes);
		
	
		btnBusca.setIcon(new ImageIcon(frmCadastraColetor.class.getResource("/Images/search.png")));
		btnBusca.setBackground(new Color(40, 40,40));
		btnBusca.setBounds(581, 17, 35, 35);
		jpanel.add(btnBusca);
		
		txtBairro = new JTextField();
		txtBairro.setBounds(643, 242, 220, 24);
		jpanel.add(txtBairro);
		
		
		JComboBox cbSexo = new JComboBox();
		cbSexo.setBackground(Color.WHITE);
		cbSexo.setBounds(817, 112, 46, 24);
		jpanel.add(cbSexo);
		cbSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "F", "M"}));
		
		JComboBox cbUF = new JComboBox();
		cbUF.setBackground(Color.WHITE);
		cbUF.setBounds(811, 202, 52, 24);
		cbUF.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR", "RJ", "RN", "RO", "RS", "SC", "SE", "SP", "TO"}));
		jpanel.add(cbUF);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(233, 284, 630, 24);
		jpanel.add(txtEmail);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setForeground(Color.WHITE);
		lblEmail.setFont(new Font("Khmer OS", Font.BOLD, 14));
		lblEmail.setBounds(111, 288, 46, 26);
		jpanel.add(lblEmail);
	
		
		// Listeners dos botões		
		
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (index > 0) {
					index --;
					fillFields(index, cbSexo, cbUF);
					
				}
			}
		});
		
		btnProx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(index != funcionarios.size() - 1 ) {
					index++;
					fillFields(index, cbSexo, cbUF);
					
				}
				
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fillFields(index, cbSexo,cbUF);
				enableFields(false,cbSexo,cbUF);
				btnAnterior.setVisible(true);
				btnProx.setVisible(true);
				btnAdd.setVisible(true);
				btnEdit.setVisible(true);	
				btnBusca.setVisible(true);
				btnSave.setVisible(false);
				btnCancel.setVisible(false);
			}
		});
		
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update = true;
				enableFields(true,cbSexo,cbUF);
				btnAnterior.setVisible(false);
				btnProx.setVisible(false);
				btnAdd.setVisible(false);
				btnEdit.setVisible(false);	
				btnBusca.setVisible(false);
				btnSave.setVisible(true);
				btnCancel.setVisible(true);
				
			}
		});

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean validCPF = utilidades.isValidCPF(txtCpf.getText()); //VALIDAR CPF AQUI
				//boolean validCEP = true; // VALIDAR cEP AQUI 
				if(validCPF){
					if(txtNome.getText() == "" || txtRua.getText() == "" || txtCidade.getText() == "" || txtNumero.getText() == "" || txtEmail.getText() == "") {
						UsuarioDao db = new UsuarioDao();
						Usuário f = new Usuário();
						f.setNome(txtNome.getText());
						f.setSexo(cbSexo.getSelectedItem().toString());
						f.setCpf(txtCpf.getText());
						f.setRg(txtRg.getText());
						f.setRua(txtRua.getText());
						f.setNumero( Integer.parseInt(txtNumero.getText()));
						f.setUf(cbUF.getSelectedItem().toString());
						f.setCidade(txtCidade.getText());
						f.setCep(txtCEP.getText());
						f.setBairro(txtBairro.getText());
						f.setId(Integer.parseInt(txtId.getText()));
						f.setEmail(txtEmail.getText());
						if(update) {
							db.update(f);
							update = false;
						}else {
							db.inserir(f);
							index = funcionarios.size();
						}
						enableFields(false,cbSexo,cbUF);
						btnAnterior.setVisible(true);
						btnProx.setVisible(true);
						btnAdd.setVisible(true);
						btnEdit.setVisible(true);	
						btnBusca.setVisible(true);
						btnSave.setVisible(false);
						btnCancel.setVisible(false);
						funcionarios = db.get();						
					}else {
						JOptionPane.showMessageDialog(null, "Há campos obrigatórios vazios");
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "CPF Invalido");
				}
			}
		});
		
		
		
		// Seta visibilidade inicial dos botões
		
		btnCancel.setVisible(false);
		btnSave.setVisible(false);
		btnEdit.setVisible(false);
		btnAdd.setVisible(true);
		btnBusca.setVisible(true);
		
		if(isNew) {
			clearFields(cbSexo,cbUF);
			enableFields(true,cbSexo,cbUF);	
			txtId.setText( Integer.toString(funcionarios.size()+1));
			btnAnterior.setVisible(false);
			btnProx.setVisible(false);
			btnAdd.setVisible(false);
			btnBusca.setVisible(false);
			btnSave.setVisible(true);
			btnCancel.setVisible(true);
		}else {
			// Desabilita os campos de texto
			enableFields(false,cbSexo,cbUF);
			
			
			// Carrega os dados dos funcionarios se houver
			if(funcionarios.size() >0 ) {
				fillFields(index,cbSexo,cbUF);	
				
			}			
		}
		
		
		
	}
}
