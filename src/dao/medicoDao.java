package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.Usuário;
import classes.medico;
import classes.Coletor;
import classes.medico;

public class medicoDao {
	
	private Connection connection;
	
	public medicoDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(medico m) {
		String sql = "INSERT INTO ProfissionaisDeSaude( CPF, RG, nome, "
				 + "sexo, rua, numero, UF, CEP,cidade,bairro ,numeroDeRegistro,especialidade, idProfisional, tipoReg) VALUES (?,?,?,?,?,?,?,?,?, ?,?,?,?, ?);";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, m.getCpf());
			stmt.setString(2,m.getRg());
			stmt.setString(3, m.getNome());
			stmt.setString(4, m.getSexo());
			stmt.setString(5, m.getRua());
			stmt.setInt(6, m.getNumero());
			stmt.setString(7, m.getUf());
			stmt.setString(8, m.getCep());
			stmt.setString(9, m.getCidade());
			stmt.setString(10, m.getBairro());
			stmt.setString(11, m.getNumReg());
			stmt.setString(12, m.getEspecialidade());
			stmt.setString(13, m.getTipoReg());
			stmt.setInt(14, m.getId());
			
			
			stmt.execute();
			stmt.close();
			

			
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void update(medico m) {
		String sql = "UPDATE ProfissionaisDeSaude SET CPF = ?, RG = ?, nome = ?, "
				 + "sexo = ?, rua = ?, numero = ?, UF = ?, CEP = ?,cidade  = ?,bairro  = ?, numeroDeRegistro = ?, especialidade = ? , tipoReg = ?  WHERE idProfisional = ?";
		
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, m.getCpf());
			stmt.setString(2,m.getRg());
			stmt.setString(3, m.getNome());
			stmt.setString(4, m.getSexo());
			stmt.setString(5, m.getRua());
			stmt.setInt(6, m.getNumero());
			stmt.setString(7, m.getUf());
			stmt.setString(8, m.getCep());
			stmt.setString(9, m.getCidade());
			stmt.setString(10, m.getBairro());
			stmt.setString(11, m.getNumReg());
			stmt.setString(12, m.getEspecialidade());
			stmt.setString(13, m.getTipoReg());
			stmt.setInt(14, m.getId());
			
			
			stmt.execute();
			stmt.close();
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public List<medico> get() {
		List<medico> medicos = new ArrayList<medico>();
		try {
			String sql = "SELECT * from ProfissionaisDeSaude";
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();			
			while (rs.next()) {
				medico medico = new medico();
				medico.setId(rs.getInt("idProfisional"));
				medico.setNome(rs.getString("nome"));
				medico.setSexo(rs.getString("sexo"));
				medico.setCpf(rs.getString("CPF"));
				medico.setRg(rs.getString("RG"));
				medico.setRua(rs.getString("rua"));
				medico.setNumero(rs.getInt("numero"));
				medico.setUf(rs.getString("UF"));
				medico.setCep(rs.getString("CEP"));
				medico.setCidade(rs.getString("cidade"));
				medico.setBairro(rs.getString("bairro"));
				medico.setTipoReg(rs.getString("tipoReg"));
				medico.setNumReg(rs.getString("numeroDeRegistro"));
				medico.setEspecialidade(rs.getString("especialidade"));
				medicos.add(medico);
			
			}
			
			stmt.close();
			
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return medicos;
	}
	
	public List<medico> getByField(String field, String term) {
		List<medico> medicos = new ArrayList<medico>();
		try {
			String sql = "";			
			if(field == "Nome") {
				sql = "SELECT * from ProfissionaisDeSaude WHERE nome like ?";
			}else if(field == "CPF") {
				sql = "SELECT * from ProfissionaisDeSaude WHERE CPF like ?";
			}else {
				sql = "SELECT * from ProfissionaisDeSaude WHERE idProfisional like ?";
			 
			}		
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, "%" + term + "%");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				medico medico = new medico();
				medico.setId(rs.getInt("idProfisional"));
				medico.setNome(rs.getString("nome"));
				medico.setSexo(rs.getString("sexo"));
				medico.setCpf(rs.getString("CPF"));
				medico.setRg(rs.getString("RG"));
				medico.setRua(rs.getString("rua"));
				medico.setNumero(rs.getInt("numero"));
				medico.setUf(rs.getString("UF"));
				medico.setCep(rs.getString("CEP"));
				medico.setCidade(rs.getString("cidade"));
				medico.setBairro(rs.getString("bairro"));
				medico.setTipoReg(rs.getString("tipoReg"));
				medico.setNumReg(rs.getString("numeroDeRegistro"));
				medico.setEspecialidade(rs.getString("especialidade"));
				medicos.add(medico);
			
			}
			
			stmt.close();
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return medicos;
	}
	
	
	public medico getByRegistro(String registro) {
		medico medico = new medico();
		try {			
			String sql = "SELECT * from ProfissionaisDeSaude WHERE numeroDeRegistro == ?";			
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, registro);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				medico.setId(rs.getInt("idProfisional"));
				medico.setNome(rs.getString("nome"));
				medico.setSexo(rs.getString("sexo"));
				medico.setCpf(rs.getString("CPF"));
				medico.setRg(rs.getString("RG"));
				medico.setRua(rs.getString("rua"));
				medico.setNumero(rs.getInt("numero"));
				medico.setUf(rs.getString("UF"));
				medico.setCep(rs.getString("CEP"));
				medico.setCidade(rs.getString("cidade"));
				medico.setBairro(rs.getString("bairro"));
				medico.setTipoReg(rs.getString("tipoReg"));
				medico.setNumReg(rs.getString("numeroDeRegistro"));
				medico.setEspecialidade(rs.getString("especialidade"));
				break;
			}
			stmt.close();
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return medico;
	}
	
}
