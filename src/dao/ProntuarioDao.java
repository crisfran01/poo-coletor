package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.Prontuario;


public class ProntuarioDao {
	
	private Connection connection;
	
	public ProntuarioDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(Prontuario p) {
		String sql = "INSERT INTO registroProntuario(idregistroProntuario, idPaciente, idProfissionaldasaude, resumo, anotacoes) VALUES (?,?,?,?,?);";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, p.getIdregistroProntuario());
			stmt.setInt(2,p.getIdPaciente());
			stmt.setInt(3, p.getIdProfissionaldasaude());
			stmt.setString(4, p.getResumo());
			stmt.setString(5, p.getAnotacoes());
			
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void update(Prontuario p) {
		String sql = "UPDATE registroProntuario  SET  idPaciente = ?, idProfissionaldasaude = ?, resumo = ?, anotacoes = ? ) WHERE idregistroProntuario = ?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1,p.getIdPaciente());
			stmt.setInt(2, p.getIdProfissionaldasaude());
			stmt.setString(3, p.getResumo());
			stmt.setString(4, p.getAnotacoes());
			stmt.setInt(5, p.getIdregistroProntuario());
			
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void remove(Prontuario p) {
		String sql = "DELETE FROM registroProntuario WHERE idregistroProntuario = ?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, p.getIdregistroProntuario());
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public List<Prontuario> get() {
		List<Prontuario> prontuarios = new ArrayList<Prontuario>();
		try {
			String sql = "SELECT r.*,p.nome FROM registroProntuario r join Paciente p on r.idPaciente = p.idPaciente";
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();			
			while (rs.next()) {
				Prontuario prontuario = new Prontuario();
				prontuario.setIdregistroProntuario( rs.getInt("idregistroProntuario"));
				prontuario.setIdPaciente(rs.getInt("idPaciente"));
				prontuario.setIdProfissionaldasaude(rs.getInt("idProfissionaldasaude"));
				prontuario.setResumo(rs.getString("resumo"));
				prontuario.setAnotacoes(rs.getString("anotacoes"));
				prontuario.setNomePaciente(rs.getString("nome"));
				prontuarios.add(prontuario);
			
			}
			stmt.close();
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return prontuarios;
	}
	
	public List<Prontuario> getByField(String field, String term) {
		List<Prontuario> prontuarios = new ArrayList<Prontuario>();
		try {
			String sql = "";			
			if(field == "Nome") {
				sql = "SELECT r.*,p.nome FROM registroProntuario r join Paciente p on r.idPaciente = p.idPaciente WHERE p.nome like ?";
			}else {
				sql = "SELECT r.*,p.nome FROM registroProntuario r join Paciente p on r.idPaciente = p.idPaciente WHERE p.idPaciente like ?";
			
			}		
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, "%" + term + "%");
			ResultSet rs = stmt.executeQuery();		
			while (rs.next()) {
				Prontuario prontuario = new Prontuario();
				prontuario.setIdregistroProntuario( rs.getInt("idregistroProntuario"));
				prontuario.setIdPaciente(rs.getInt("idPaciente"));
				prontuario.setIdProfissionaldasaude(rs.getInt("idProfissionaldasaude"));
				prontuario.setResumo(rs.getString("resumo"));
				prontuario.setAnotacoes(rs.getString("anotacoes"));
				prontuario.setNomePaciente(rs.getString("nome"));
				prontuarios.add(prontuario);
			}		
				
			stmt.close();
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return prontuarios;
	}
}
