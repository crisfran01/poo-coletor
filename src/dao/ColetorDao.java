package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.Usuário;
import classes.Coletor;

public class ColetorDao {
	
	private Connection connection;
	
	public ColetorDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(Coletor p) {
		String sql = "INSERT INTO Coletor( CPF, RG, nome, "
				 + "sexo, rua, numero, UF, CEP,cidade,bairro ,convenio, material, email) VALUES (?,?,?,?,?,?,?,?,?, ?,?,?);";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, p.getCpf());
			stmt.setString(2,p.getRg());
			stmt.setString(3, p.getNome());
			stmt.setString(4, p.getSexo());
			stmt.setString(5, p.getRua());
			stmt.setInt(6, p.getNumero());
			stmt.setString(7, p.getUf());
			stmt.setString(8, p.getCep());
			stmt.setString(9, p.getCidade());
			stmt.setString(10, p.getBairro());
			stmt.setString(11, p.getMaterial());
			stmt.setString(12, p.getEmail());
			
			stmt.execute();
			stmt.close();
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	
	public void update(Coletor p) {
		String sql = "UPDATE Coletor SET CPF = ?, RG = ?, nome = ?, "
				 + "sexo = ?, rua = ?, numero = ?, UF = ?, CEP = ?,cidade  = ?,bairro  = ? ,material = ?, email = ? WHERE idColetor = ?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, p.getCpf());
			stmt.setString(2,p.getRg());
			stmt.setString(3, p.getNome());
			stmt.setString(4, p.getSexo());
			stmt.setString(5, p.getRua());
			stmt.setInt(6, p.getNumero());
			stmt.setString(7, p.getUf());
			stmt.setString(8, p.getCep());
			stmt.setString(9, p.getCidade());
			stmt.setString(10, p.getBairro());
			stmt.setString(11, p.getMaterial());
			stmt.setString(12, p.getEmail());
			stmt.setInt(13, p.getId());
			
			stmt.execute();
			stmt.close();
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	
	public List<Coletor> get() {
		List<Coletor> coletores = new ArrayList<Coletor>();
		try {
			String sql = "SELECT * FROM Coletor";
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();			
			while (rs.next()) {
				Coletor coletor = new Coletor();
				coletor.setId(rs.getInt("idColetor"));
				coletor.setNome(rs.getString("nome"));
				coletor.setSexo(rs.getString("sexo"));
				coletor.setCpf(rs.getString("CPF"));
				coletor.setRg(rs.getString("RG"));
				coletor.setRua(rs.getString("Rua"));
				coletor.setNumero(rs.getInt("numero"));
				coletor.setUf(rs.getString("UF"));
				coletor.setCep(rs.getString("CEP"));
				coletor.setCidade(rs.getString("cidade"));
				coletor.setBairro(rs.getString("bairro"));
				coletor.setMaterial(rs.getString("material"));
				coletor.setEmail(rs.getString("email"));
				coletores.add(coletor);
			
			}
			stmt.close();
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return coletores;
	}
	
	public List<Coletor> getByField(String field, String term) {
		List<Coletor> coletores = new ArrayList<Coletor>();
		try {
			String sql = "";			
			if(field == "Nome") {
				sql = "SELECT * FROM Coletor WHERE nome like ?";
			}else if(field == "CPF") {
				sql = "SELECT * FROM Coletor WHERE CPF like ?";
			}else {
				sql = "SELECT * FROM Coletor WHERE idColetor like ?";
			
			}		
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, "%" + term + "%");
			ResultSet rs = stmt.executeQuery();		
			while (rs.next()) {
				Coletor coletor = new Coletor();
				coletor.setId(rs.getInt("idColetor"));
				coletor.setNome(rs.getString("nome"));
				coletor.setSexo(rs.getString("sexo"));
				coletor.setCpf(rs.getString("CPF"));
				coletor.setRg(rs.getString("RG"));
				coletor.setRua(rs.getString("Rua"));
				coletor.setNumero(rs.getInt("numero"));
				coletor.setUf(rs.getString("UF"));
				coletor.setCep(rs.getString("CEP"));
				coletor.setCidade(rs.getString("cidade"));
				coletor.setBairro(rs.getString("bairro"));
				coletor.setMaterial(rs.getString("material"));
				coletor.setEmail(rs.getString("email"));
				coletores.add(coletor);			
			}		
			
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return coletores;
	}
	
	
	
}
