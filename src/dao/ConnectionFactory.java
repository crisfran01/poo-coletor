package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	public Connection getConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
			return DriverManager.getConnection("jdbc:sqlite:/home/cristiane/Documents/Projects/healthSystem/HealthSystem/src/dao/health.db");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
