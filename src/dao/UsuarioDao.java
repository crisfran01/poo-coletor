package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.Prontuario;
import classes.Usuário;
import classes.Coletor;

public class UsuarioDao {
	
	private Connection connection;
	
	public UsuarioDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(Usuário f) {
		String sql = "INSERT INTO Usuarios(idUsuario, CPF, RG, nome, "
				 + "sexo, rua, numero, UF, CEP,cidade, bairro, email)"
				 + " VALUES (?,?,?,?,?,?,?,?,?,?,?), ?;";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, f.getId());
			stmt.setString(2, f.getCpf());
			stmt.setString(3,f.getRg());
			stmt.setString(4, f.getNome());
			stmt.setString(5, f.getSexo());
			stmt.setString(6, f.getRua());
			stmt.setInt(7, f.getNumero());
			stmt.setString(8, f.getUf());
			stmt.setString(9, f.getCep());
			stmt.setString(10, f.getCidade());
			stmt.setString(11, f.getBairro());
			stmt.setString(12, f.getEmail());


			stmt.execute();
			stmt.close();
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void update(Usuário p) {
		String sql = "UPDATE Usuarios SET CPF = ?, RG = ?, nome = ?, "
				 + "sexo = ?, rua = ?, numero = ?, UF = ?, CEP = ?,cidade  = ?,bairro  = ?, email = ? WHERE idUsuario = ?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1, p.getCpf());
			stmt.setString(2,p.getRg());
			stmt.setString(3, p.getNome());
			stmt.setString(4, p.getSexo());
			stmt.setString(5, p.getRua());
			stmt.setInt(6, p.getNumero());
			stmt.setString(7, p.getUf());
			stmt.setString(8, p.getCep());
			stmt.setString(9, p.getCidade());
			stmt.setString(10, p.getBairro());
			stmt.setString(11, p.getEmail());
			stmt.setInt(12, p.getId());
			
			stmt.execute();
			stmt.close();
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	

	
	public List<Usuário> get() {
		List<Usuário> usuarios = new ArrayList<Usuário>();
		try {
			String sql = "SELECT * FROM Usuarios";
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();			
			while (rs.next()) {
				Usuário usuario = new Usuário();
				usuario.setId(rs.getInt("idUsuario"));
				usuario.setNome(rs.getString("nome"));
				usuario.setSexo(rs.getString("sexo"));
				usuario.setCpf(rs.getString("CPF"));
				usuario.setRg(rs.getString("RG"));
				usuario.setRua(rs.getString("Rua"));
				usuario.setNumero(rs.getInt("numero"));
				usuario.setUf(rs.getString("UF"));
				usuario.setCep(rs.getString("CEP"));
				usuario.setCidade(rs.getString("cidade"));
				usuario.setBairro(rs.getString("bairro"));
				usuario.setEmail(rs.getString("email"));
				usuarios.add(usuario);
			
			}
			stmt.close();
			
			
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return usuarios;
	}
	
	public List<Usuário> getByField(String field, String term) {
		List<Usuário> usuarios = new ArrayList<Usuário>();
		try {
			String sql = "";			
			if(field == "Nome") {
				sql = "SELECT * FROM Usuarios WHERE nome like ?";
			}else if(field == "ID"){
				sql = "SELECT * FROM Usuarios WHERE idUsuario like ?";
			}else if (field == "CPF"){
				sql = "SELECT * FROM Usuarios WHERE CPF like ?";
			}
			
			PreparedStatement stmt = this.connection.prepareStatement(sql);
			stmt.setString(1, "%" + term + "%");
			ResultSet rs = stmt.executeQuery();		
			while (rs.next()) {
				Usuário usuario = new Usuário();
				usuario.setId(rs.getInt("idUsuario"));
				usuario.setNome(rs.getString("nome"));
				usuario.setSexo(rs.getString("sexo"));
				usuario.setCpf(rs.getString("CPF"));
				usuario.setRg(rs.getString("RG"));
				usuario.setRua(rs.getString("Rua"));
				usuario.setNumero(rs.getInt("numero"));
				usuario.setUf(rs.getString("UF"));
				usuario.setCep(rs.getString("CEP"));
				usuario.setCidade(rs.getString("cidade"));
				usuario.setBairro(rs.getString("bairro"));
				usuario.setEmail(rs.getString("email"));
				usuarios.add(usuario);
			}	
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
		
		return usuarios;
	}
}

