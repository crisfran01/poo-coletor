package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classes.exame;
import classes.Coletor;

public class exameDao {
	
	private Connection connection;
	public exameDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(exame e) {
		String sql = "INSERT INTO exame( idExame,idPaciente,idProfissionaldasaude,data,tipo,"
				+ "observacao,idUsuario) VALUES (?,?,?,?,?,?,?);";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, e.getIdExame());
			stmt.setInt(2,e.getIdPaciente());
			stmt.setInt(3, e.getIdProfissionalDeSaude());
			stmt.setString(4, e.getData());
			stmt.setString(5, e.getTipo());
			stmt.setString(6, e.getObservacao());
			stmt.setInt(7, e.getIdUsuario());
			
			stmt.execute();
			stmt.close();
		}catch( SQLException q) {
			q.printStackTrace();
		};
	}
	
	public void update(exame e) {
		String sql = "UPDATE exame SET = idExame=?,idPaciente=?,idProfissionaldasaude=?,data=?,tipo=?"
				+ "observacao=?,idUsuario=? where idExame = ?";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, e.getIdExame());
			stmt.setInt(2,e.getIdPaciente());
			stmt.setInt(3, e.getIdProfissionalDeSaude());
			stmt.setString(4, e.getData());
			stmt.setString(5, e.getTipo());
			stmt.setString(6, e.getObservacao());
			stmt.setInt(7, e.getIdUsuario());
			
			stmt.execute();
			stmt.close();
		}catch( SQLException q) {
			q.printStackTrace();
		};
	}
		
		public List<exame> get() {
			List<exame> exames = new ArrayList<exame>();
			try {
				String sql = "SELECT e.*, p.nome FROM exame e join Paciente p on e.idPaciente = p.idPaciente";
				PreparedStatement stmt = this.connection.prepareStatement(sql);
				ResultSet rs = stmt.executeQuery();			
				while (rs.next()) {
					exame exame = new exame();
					exame.setIdExame(rs.getInt("idExame"));
					exame.setIdPaciente(rs.getInt("idPaciente"));
					exame.setIdProfissionalDeSaude(rs.getInt("idProfissionaldasaude"));
					exame.setData(rs.getString("data"));
					exame.setTipo(rs.getString("tipo"));
					exame.setObservacao(rs.getString("observacao"));
					exame.setIdUsuario(rs.getInt("idUsuario"));
					exame.setNomePaciente(rs.getString("nome"));
					exames.add(exame);
					
				}
				stmt.close();
				
				
			}catch( SQLException q) {
				q.printStackTrace();
			};
			
			return exames;
		}
		
		public List<exame> getByField(String field,String term) {
			List<exame> exames = new ArrayList<exame>();
			try {
				String sql = "";			
				if(field == "Nome") {
					sql = "SELECT e.*, p.nome FROM exame e join Paciente p on e.idPaciente = p.idPaciente WHERE p.nome like ?";
				}else if(field == "Prontuario") {
					sql = "SELECT e.*, p.nome FROM exame e join Paciente p on e.idPaciente = p.idPaciente WHERE e.idPaciente like  ?";
				}else {
					sql = "SELECT e.*, p.nome FROM exame e join Paciente p on e.idPaciente = p.idPaciente WHERE e.idExame like  ?";
				}
				PreparedStatement stmt = this.connection.prepareStatement(sql);
				stmt.setString(1, "%" + term + "%");
				ResultSet rs = stmt.executeQuery();			
				while (rs.next()) {
					exame exame = new exame();
					exame.setIdExame(rs.getInt("idExame"));
					exame.setIdPaciente(rs.getInt("idPaciente"));
					exame.setIdProfissionalDeSaude(rs.getInt("idProfissionaldasaude"));
					exame.setData(rs.getString("data"));
					exame.setTipo(rs.getString("tipo"));
					exame.setObservacao(rs.getString("observacao"));
					exame.setIdUsuario(rs.getInt("idUsuario"));
					exame.setNomePaciente(rs.getString("nome"));
					exames.add(exame);
				}
				
				stmt.close();
			}catch( SQLException q) {
				q.printStackTrace();
			};
			
			return exames;
		}
}

