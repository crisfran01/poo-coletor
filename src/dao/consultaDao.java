package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import classes.consulta;
import classes.Usuário;
import classes.Coletor;

public class consultaDao {
	
	private Connection connection;
	
	public consultaDao() {
		connection = new ConnectionFactory().getConnection();
	}
	
	public void inserir(consulta p) {
		String sql = "INSERT INTO Consulta(data, hora, sala, \"\n" + 
				"				 + \"observacao, idProfissional, idPaciente) VALUES (?,?,?,?,?,?);";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setDate(1, p.getData());
			stmt.setTime(2,p.getHora());
			stmt.setString(3, p.getSala());
			stmt.setString(4, p.getObservacao());
			stmt.setInt(5, p.getIdProfissional());
			stmt.setInt(6, p.getIdPaciente());
			
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void update(consulta p) {
		String sql = "UPDATE Consulta  SET  data = ?, hora = ?, sala = ?, \"\n" + 
				"				 + \"observacao = ?, idProfissional = ?, idPaciente = ?) WHERE idConsulta = ?;";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setDate(1, p.getData());
			stmt.setTime(2,p.getHora());
			stmt.setString(3, p.getSala());
			stmt.setString(4, p.getObservacao());
			stmt.setInt(5, p.getIdProfissional());
			stmt.setInt(6, p.getIdPaciente());
			stmt.setInt(7, p.getId());
			
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
	public void remove(consulta p) {
		String sql = "DELETE FROM Consulta WHERE idConsulta = ?;";
		
		try {
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setInt(1, p.getId());
			stmt.execute();
			stmt.close();
						
		}catch( SQLException e) {
			e.printStackTrace();
		};
	}
	
}
